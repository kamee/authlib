package main

import (
	"log"
	"fmt"
	"time"
	"bytes"
	"math/rand"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"encoding/base64"
)

type Header struct {
	Alg string  `json:"alg"`
	Typ string	`json:"type"`
}

type Payload struct {
	Username	string	`json:"username"`
	Admin		bool	`json:"admin"`
	Iat			int64	`json:"iat"`
	Exp			int64	`json:"exp"`
}

func generateSecretKey()(string, error){
	// change this part: generate random&secure password
	number := rand.Int()

	return string(number), nil
}


func toBase64(h interface{}) (string, error){
	var buf bytes.Buffer
	encoder := base64.NewEncoder(base64.URLEncoding, &buf)
	err := json.NewEncoder(encoder).Encode(h)
	if err != nil {
		return "", err
	}

	encoder.Close()
	return buf.String(), nil
}


func main(){
	registeredTime := time.Now().Unix()
	expirationDate := time.Now().Add(time.Hour * 2).Unix()

	header := Header{
		"HS256",
		"JWT",
	}

	payload := Payload{
		Username: "username",
		Admin: true,
		Iat: registeredTime,
		Exp: expirationDate,
	}

	headerEnc, err := toBase64(header)
	if err != nil{
		log.Fatal(err)
	}

	payloadEnc, err := toBase64(payload)
	if err != nil{
		log.Fatal(err)
	}

	encoded := headerEnc + "." + payloadEnc
	secret, err := generateSecretKey()
	if err != nil{
		log.Fatal(err)
	}

	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(encoded))
	signature := hex.EncodeToString(h.Sum(nil))
	signatureEnc, err := toBase64(signature)

	jwt_token := headerEnc + "." + payloadEnc + "." + signatureEnc
	fmt.Println(jwt_token)
}
